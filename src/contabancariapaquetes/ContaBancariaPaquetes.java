
package contabancariapaquetes;

import titular.Titular;


public class ContaBancariaPaquetes {
    private String numConta;
    private float saldo;
    private Titular persoa;//variable referenciada
    
    public ContaBancariaPaquetes(){
    
    }
    
    public ContaBancariaPaquetes(String numConta,float saldo,String nome,long dni){
    this.numConta=numConta;
    this.saldo=saldo;
    persoa=new Titular();
    persoa.setDni(dni);
    persoa.setNome(nome);
    }
    
    public void accesoOutroPaquete(){
    Titular obx =new Titular();
        System.out.println(obx.getDni());
    }

    public String getNumConta() {
        return numConta;
    }

   
    public void setNumConta(String numConta) {
        this.numConta = numConta;
    }

   
    public float getSaldo() {
        return saldo;
    }

    
    public void setSaldo(float saldo) {
        this.saldo = saldo;
    }


    public Titular getPersoa() {
        return persoa;
    }

    public void setPersoa(Titular persoa) {
        this.persoa = persoa;
    }
    
}
