
package titular;


public class Titular {
   private String nome;
   private long dni; //debría ser String 
    
  public Titular(){
  
  }
  
  public Titular(String nome, long dni){
  this.nome=nome;
  this.dni=dni;
   
  }
  
 public Titular devolverDatos(){
 
     return this;//Devolver o obxeto
 }
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public long getDni() {
        return dni;
    }

    public void setDni(long dni) {
        this.dni = dni;
    }
  
    
}
